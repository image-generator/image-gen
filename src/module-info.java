/**
 * @author Samuel Segal
 */
module imagegen {
	
	requires transitive java.desktop;
	
	requires transitive javafx.controls;
	requires javafx.graphics;
	
	requires transitive imagegenlib;


	exports imagegen;
	exports imagegen.reflection;
	exports imagegen.scenes;
	exports imagegen.render;
	exports imagegen.paramfields;
}