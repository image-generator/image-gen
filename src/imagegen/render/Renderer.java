package imagegen.render;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

import javax.imageio.ImageIO;

import imagegen.reflection.GenClassReflect;
import imagegenlib.generator.IGenerator;
import javafx.scene.image.Image;


public class Renderer {

	protected Consumer<Image> imageUpdate = System.out::println;
	
	protected GenClassReflect genReflected;
	protected IGenerator invoker;
	
	protected Thread renderThread = null;
	protected boolean canThreadContinue = true;
	
	protected BufferedImage resultImage;
	
	
	public Renderer(GenClassReflect genClass) {
		this.genReflected = genClass;
		
	}
	
	
	public void initialize(Object[] args) {
		this.invoker = this.genReflected.initialize(args);
	}
	
	
	public void start() {
		if(this.renderThread==null
			||!this.renderThread.isAlive()) {
			this.canThreadContinue = true;
			this.renderThread = new Thread() {
				@Override
				public void run() {
					render();
				}
			};
			this.renderThread.start();
		}
	}
	
	public void stop() {
		this.canThreadContinue = false;
	}
	
	
	private void render() {
		int width = this.invoker.getWidth();
		int height= this.invoker.getHeight();
		
		BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
		for(int y=0;y<height&&this.canThreadContinue;y++) {
			for(int x=0;x<width&&this.canThreadContinue;x++) {
				
				int result = this.invoker.getColor(x, y);
				image.setRGB(x, y, result);
				
			}
			try {
				
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				ImageIO.write(image, "png", os);
				InputStream is = new ByteArrayInputStream(os.toByteArray());
				Image outImage = new Image(is);
				
				onImageUpdate(outImage);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		this.resultImage = image;
	}
	
	
	public void onImageUpdate(Image img) {
		this.imageUpdate.accept(img);
	}
	
	
	public void setOnImageUpdate(Consumer<Image> imageUpdate) {
		this.imageUpdate = imageUpdate;
	}
	
	public Class<?> getGenClass() {
		return this.genReflected.getClass();
	}
	
	public GenClassReflect getGenReflected() {
		return genReflected;
	}
	
	public BufferedImage getResultImage() {
		return resultImage;
	}
	
	
}
