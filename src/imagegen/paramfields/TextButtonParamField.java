package imagegen.paramfields;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;


public abstract class TextButtonParamField extends ParamField{

	protected HBox hbox = new HBox();
	protected TextField entry = new TextField();
	protected Button searchButton = new Button();
	
	
	public TextButtonParamField(String name) {
		super(name);
		
		this.searchButton.setOnMouseClicked(this::onButtonClick);
		
		hbox.getChildren().addAll(entry,searchButton);
	}
	
	
	abstract void onButtonClick(MouseEvent event);

	
	@Override
	public Node getLabel() {
		return this.hbox;
	}

	

}
