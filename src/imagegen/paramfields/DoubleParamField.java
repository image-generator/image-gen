package imagegen.paramfields;


public class DoubleParamField extends TextParamField{

	

	public DoubleParamField(String name) {
		super(name);
	}

	@Override
	public Double getResult() {
		return Double.parseDouble(this.field.getText());
	}

	@Override
	public Class<?> getResultClass() {
		return Double.class;
	}

}
