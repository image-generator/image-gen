package imagegen.paramfields;


public class IntegerParamField extends TextParamField{

	public IntegerParamField(String name) {
		super(name);
	}

	@Override
	public Object getResult() {
		return Integer.parseInt(this.field.getText());
	}

	@Override
	public Class<?> getResultClass() {
		return Integer.class;
	}

}
