package imagegen.paramfields;

import javafx.scene.control.TextField;


public abstract class TextParamField extends ParamField{
	
	protected TextField field;
	
	public TextParamField(String name) {
		super(name);
		this.field = new TextField();
	}
	
	@Override
	public TextField getLabel() {
		return field;
	}
}
