package imagegen.paramfields;

import javafx.scene.Node;


public abstract class ParamField{

	private String name;
	
	
	public ParamField(String name) {
		this.name = name;
	}
	
	
	public abstract Node getLabel();
	
	public abstract Object getResult();
	
	public abstract Class<?> getResultClass();
	
	
	public String getName() {
		return name;
	}
}
