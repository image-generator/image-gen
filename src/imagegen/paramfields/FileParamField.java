package imagegen.paramfields;

import java.io.File;
import java.io.IOException;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


public class FileParamField extends ParamField{

	protected HBox vbox = new HBox();
	protected TextField entry = new TextField();
	protected Button searchButton = new Button();
	
	public FileParamField(String name) {
		super(name);
		
		searchButton.setOnMouseClicked((event)->{
			Stage stage = (Stage)vbox.getScene().getWindow();
			
			FileChooser filer = new FileChooser();
			filer.setTitle("Choose an file");
			File file = filer.showOpenDialog(stage);
			try {
				this.entry.setText(file.getCanonicalPath());
			} catch (IOException e) {
				this.entry.setText("Error!");
				e.printStackTrace();
			}
		});
		
		vbox.getChildren().add(entry);
		vbox.getChildren().add(searchButton);
		
	}

	@Override
	public Node getLabel() {
		return this.vbox;
	}

	@Override
	public String getResult() {
		return this.entry.getText();
	}

	@Override
	public Class<?> getResultClass() {
		return String.class;
	}

}
