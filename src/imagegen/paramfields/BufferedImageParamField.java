package imagegen.paramfields;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import imagegen.nodes.ImageFileChooser;
import javafx.scene.input.MouseEvent;


public class BufferedImageParamField extends TextButtonParamField{

	private File file;
	private BufferedImage image;
	
	public BufferedImageParamField(String name) {
		super(name);
	}


	@Override
	public BufferedImage getResult() {
		return this.image;
	}

	@Override
	public Class<?> getResultClass() {
		return BufferedImage.class;
	}

	@Override
	void onButtonClick(MouseEvent event) {

		this.file = ImageFileChooser.openImageFile(this.hbox.getScene().getWindow());
		this.entry.setText(this.file.getName());
		
		try {
			this.image = ImageIO.read(this.file);
		} catch (IOException e) {
			this.image = null;
		}
		
	}

}
