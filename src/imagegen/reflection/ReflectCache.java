package imagegen.reflection;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;


public class ReflectCache {

	public List<GenClassReflect> classCache = new ArrayList<GenClassReflect>();

	public void initializeClasses()  {

		try {

			File gen =  new File("generators/");

			String[] contentsStrings = gen.list();


			List<File> jars = new ArrayList<File>();
			for(String s: contentsStrings) {
				if(s.endsWith(".jar")) {

					File f = new File(gen.getCanonicalPath()+'\\'+s);

					System.out.println(f.getCanonicalPath());
					jars.add(f);
				}
			}


			for(File file:jars) {

				JarFile jarfile = new JarFile(file);
				Enumeration<JarEntry> entries = jarfile.entries();	


				URL jarUrl = new URL("jar:file:"+file+"!/");
				URL[] urls = {jarUrl};
				URLClassLoader classload = new URLClassLoader(urls);

				while(entries.hasMoreElements()) {
					JarEntry entry = entries.nextElement();
					String name = entry.getName();

					if(name.endsWith(".class")) {
						name = name
								.replace('/', '.')
								.replace(".class","");

						Class<?> c = classload.loadClass(name);


						if(GenClassReflect.isGenClass(c)) {
							GenClassReflect g = new GenClassReflect(c);
							this.classCache.add(g);
						}


					}
				}

				classload.close();
				jarfile.close();

			}




		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	
}
