package imagegen.reflection;

import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import imagegen.paramfields.BufferedImageParamField;
import imagegen.paramfields.DoubleParamField;
import imagegen.paramfields.FileParamField;
import imagegen.paramfields.IntegerParamField;
import imagegen.paramfields.ParamField;


public final class ParamGetter {


	private ParamGetter() {}
		
	public static Map<Class<?>, Class<? extends ParamField>> classMap = Map.of(
			Double.class, DoubleParamField.class,
			double.class, DoubleParamField.class,
			Integer.class, IntegerParamField.class,
			int.class, IntegerParamField.class,
			File.class, FileParamField.class,
			BufferedImage.class, BufferedImageParamField.class
	);

	
	
	public final static ParamField getParamFieldFromClass(Class<?> c,String name) throws ClassNotFoundException {
		
		if(classMap.containsKey(c)) {
			try {
				return classMap.get(c).getConstructor(String.class).newInstance(name);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		}
		throw new ClassNotFoundException("There is no input field for the given class!");
	}
	
	
	public final static boolean hasClass(Class<?> cl) {
		return ParamGetter.classMap.containsKey(cl);
	}
}
