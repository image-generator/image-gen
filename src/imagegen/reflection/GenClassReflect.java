package imagegen.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;

import imagegenlib.annotate.CanonConstructor;
import imagegenlib.annotate.GenClass;
import imagegenlib.generator.IGenerator;


public class GenClassReflect {
	
	public static boolean isGenClass(Class<?> clas) {
		boolean isAbstract = Modifier.isAbstract(clas.getModifiers());
		if(isAbstract) {
			return false;
		}
		
		boolean hasGenAnnote = clas.isAnnotationPresent(GenClass.class);
		if(!hasGenAnnote) {
			return false;
		}
		
		boolean isGenerator = IGenerator.class.isAssignableFrom(clas);
		if(!isGenerator) {
			return false;
		}
		
		for(Constructor<?> c: clas.getConstructors()) {
			if(c.isAnnotationPresent(CanonConstructor.class)) {
				return true;
			}
		}
		
		
		return false;
	}
	
	
	
	
	protected Class<?> genClass;
	protected Constructor<?> canonicalConstructor;
	
	protected String name;
	protected String description;
	
	protected GenParamReflect[] params;

	
	public GenClassReflect(Class<?> genClass) {
		this.genClass = genClass;
		
		this.establishCanonicalConstructor();
		this.establishInfo();
		this.establishParams();
	}
	
	
	protected void establishCanonicalConstructor() {
		for(Constructor<?> constructor:this.genClass.getConstructors()) {
			if(constructor.isAnnotationPresent(CanonConstructor.class)) {
				this.canonicalConstructor = constructor;
				break;
			}
		}
	}
	
	protected void establishInfo() {
		GenClass annote = genClass.getAnnotation(GenClass.class);
		this.name = annote.name();
		this.description = annote.desc();
	}
	
	protected void establishParams() {
		Parameter[] paramList = this.canonicalConstructor.getParameters();
		
		this.params = new GenParamReflect[paramList.length];
		
		for(int i=0;i<this.params.length;i++) {
			GenParamReflect p = new GenParamReflect(paramList[i]);
			this.params[i] = p;
		}
	}
	
	
	public IGenerator initialize(Object[] args) {
		
		try {
			return (IGenerator)this.canonicalConstructor.newInstance(args);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	//Typical getters, nothing to see here
	public Constructor<?> getCanonicalConstructor() {
		return canonicalConstructor;
	}
	
	public Class<?> getGenClass() {
		return genClass;
	}
	
	public GenParamReflect[] getGenParams() {
		return params;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	

	
	
}
