package imagegen.reflection;

import java.lang.reflect.Parameter;

import imagegenlib.annotate.GenArgName;


//Wrapper class around Parameter
public class GenParamReflect {

	protected Parameter genParam;
	
	protected String argName;
	
	public GenParamReflect(Parameter cls) {
		this.genParam = cls;
		this.initInfo();
	}
	

	protected void initInfo() {
		GenArgName argAnnote = this.genParam.getAnnotation(GenArgName.class);
		
		if(argAnnote!=null) {
			String name = argAnnote.name();
			
			if(name!=null) {
				this.argName = name;
				return;
			}
			
		}
		this.argName = this.getType().getSimpleName();
		
	}
	
	
	public Class<?> getType(){
		return this.genParam.getType();
	}
	
	public String getName() {
		return argName;
	}
	
	public Parameter getParam() {
		return genParam;
	}
	
	
}
