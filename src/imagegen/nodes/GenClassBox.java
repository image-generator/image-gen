package imagegen.nodes;

import java.util.function.Consumer;

import imagegen.App;
import imagegen.reflection.GenClassReflect;
import javafx.geometry.Insets;
import javafx.scene.layout.VBox;

public class GenClassBox extends VBox{


	protected Consumer<GenClassReflect> onButtonPress = System.out::println;

	public GenClassBox() {
		super();
		this.setSpacing(10);
		this.setPadding( new Insets(30,20,20,20));
		
		this.initGenClassButtons();
	}

	protected void initGenClassButtons() {
		for(GenClassReflect c:App.reflectCache.classCache) {
			GenClassButton genButton = new GenClassButton(c);

			genButton.setOnMouseClicked((event)->{


				GenClassReflect buttonClass = genButton.getGenClass();
				this.onButtonPress.accept(buttonClass);
			});



			this.getChildren().add(genButton);
		}
	}

	public void setButtonPress(Consumer<GenClassReflect> consumer) {
		this.onButtonPress = consumer;
	}

}
