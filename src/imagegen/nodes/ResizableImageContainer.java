package imagegen.nodes;

import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

public class ResizableImageContainer extends BorderPane{

	private static final Background emptyBg = new Background(new BackgroundFill[] {new BackgroundFill(Color.GRAY,null,null)});
	private static final BackgroundSize resize = new BackgroundSize(1.0d,1.0d,true,true,true,false);
	
	
	private HBox imageView;
	
	
	public ResizableImageContainer() {
		super();
		
		this.setBackground(ResizableImageContainer.emptyBg);
		
		this.imageView = new HBox();
		this.imageView.setMaxWidth(Double.MAX_VALUE);
		
		this.setCenter(this.imageView);
	}
	
	
	public void setImage(Image image) {
		BackgroundImage bgimg = new BackgroundImage(image,BackgroundRepeat.NO_REPEAT,BackgroundRepeat.NO_REPEAT,BackgroundPosition.CENTER,resize);
		this.imageView.setBackground( new Background(bgimg));
	}
}
