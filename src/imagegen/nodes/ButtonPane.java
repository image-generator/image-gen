package imagegen.nodes;

import java.awt.image.BufferedImage;

import imagegen.paramfields.ParamField;
import imagegen.render.Renderer;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;

public class ButtonPane extends TilePane{

	private static final Background panelBg = new Background(new BackgroundFill[] {new BackgroundFill(new Color(1,1,1,0.2),new CornerRadii(5,false),null)}); 
	
	private Renderer renderer;
	private ParamField[] paramFields;
	
	protected TranslucentButton startButton;
	protected TranslucentButton stopButton;
	protected TranslucentButton saveButton;
	
	public ButtonPane(Renderer renderer, ParamField[] paramFields) {
		super();
		
		this.renderer = renderer;
		this.paramFields = paramFields;
		
		this.setBackground(panelBg);
		this.setMaxWidth(Double.MAX_VALUE);
		this.setOrientation(Orientation.VERTICAL);		
		this.setAlignment(Pos.BOTTOM_CENTER);
		this.setHgap(10);
		this.setPadding(new Insets(20));
		
		this.initStartButton();
		this.initStopButton();
		this.initSaveButton();
		
		this.getChildren().addAll(this.startButton,this.stopButton,this.saveButton);
	}
	
	protected void initStartButton() {
		this.startButton = new TranslucentButton("Start");
		this.startButton.setOnMouseClicked((event)->{
			Object[] args = new Object[this.paramFields.length];

			for(int i=0;i<args.length;i++) {
				args[i]  = this.paramFields[i].getResult();
			}

			this.renderer.initialize(args);
			this.renderer.start();
		});
	}
	
	protected void initStopButton() {
		this.stopButton = new TranslucentButton("Stop");
		this.stopButton.setOnMouseClicked((event)->{
			this.renderer.stop();
		});
	}
	
	protected void initSaveButton() {
		this.saveButton = new TranslucentButton("Save");
		this.saveButton.setOnMouseClicked((event)->{
			BufferedImage image = this.renderer.getResultImage();
			ImageFileChooser.saveImage(this.getScene().getWindow(), image);
		});
	}
	
}
