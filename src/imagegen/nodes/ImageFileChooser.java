package imagegen.nodes;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;


public class ImageFileChooser {

	public static final ExtensionFilter jpgFilter = new ExtensionFilter("JPG","*.jpg","*.jpeg","*.jpe","*.jfif");
	public static final ExtensionFilter pngFilter = new ExtensionFilter("PNG","*.png");
	public static final ExtensionFilter tiffFilter = new ExtensionFilter("TIFF","*.tif","*.tiff");
	public static final ExtensionFilter heicFilter = new ExtensionFilter("HEIC","*.heic");
	
	
	public static FileChooser createImageFileChooser() {
		FileChooser fileChoose = new FileChooser();
		fileChoose.getExtensionFilters().addAll(
				jpgFilter,
				pngFilter,
				tiffFilter,
				heicFilter
				);
		fileChoose.setSelectedExtensionFilter(pngFilter);
		return fileChoose;
	}
	
	
	public static File openImageFile(Window window) {
		
		FileChooser fileChooser = ImageFileChooser.createImageFileChooser();
		fileChooser.setTitle("Open Image");
		
		return fileChooser.showOpenDialog(window);
	}
	
	public static File saveImageFile(Window window) {
		
		FileChooser fileChooser = ImageFileChooser.createImageFileChooser();
		fileChooser.setTitle("Save Image");
		
		return fileChooser.showSaveDialog(window);
	}
	
	public static BufferedImage openImage(Window window) {
		
		File file = ImageFileChooser.openImageFile(window);
		
		try {
			BufferedImage image = ImageIO.read(file);
			return image;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public static void saveImage(Window window, BufferedImage bufferedImage) {
		
		File file = ImageFileChooser.saveImageFile(window);
		
		String fileName = file.getPath();
		String ext = fileName.substring(fileName.lastIndexOf('.')+1);

		try {
			ImageIO.write(bufferedImage, ext, file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
