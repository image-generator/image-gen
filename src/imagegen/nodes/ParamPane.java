package imagegen.nodes;

import imagegen.paramfields.ParamField;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.effect.Glow;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

public class ParamPane extends GridPane{
	
	private static final Background panelBg = new Background(new BackgroundFill[] {new BackgroundFill(new Color(1,1,1,0.1),new CornerRadii(5,false),null)}); 
	
	protected ParamField[] paramFields;
	
	public ParamPane(ParamField[] params) {
		
		this.paramFields = params;
		
		this.setBackground(panelBg);
		this.setPadding(new Insets(20));
		this.setHgap(10);
		this.setVgap(20);
		
		this.initParamFields();
	
	}
	
	protected void initParamFields() {
		
		Glow glow = new Glow();
		glow.setLevel(0.1);
		
		for(int i=0;i<this.paramFields.length;i++) {
			ParamField param = this.paramFields[i];
			
			if(param==null) {
				continue;
			}
			
			Label paramLabel = new Label(param.getName());
			paramLabel.setTextFill(Color.WHITE);
			paramLabel.maxWidth(Double.MAX_VALUE);
			

			Node field = param.getLabel();
			field.setEffect(glow);

			this.add(paramLabel, 0, i);
			this.add(field, 1, i);
			
		}
		
	}
	
}
