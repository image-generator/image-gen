package imagegen.nodes;

import imagegen.reflection.GenClassReflect;
import javafx.geometry.Pos;


public class GenClassButton extends TranslucentButton{

	protected GenClassReflect genClass;
	
	public GenClassButton(GenClassReflect genClass) {
		super(genClass.getName());
		this.genClass = genClass;
		
		this.setMaxWidth(Double.MAX_VALUE);
		this.setAlignment(Pos.CENTER);
	}
	
	public GenClassReflect getGenClass() {
		return genClass;
	}
	
	
}
