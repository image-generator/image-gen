package imagegen.nodes;

import javafx.scene.Group;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.BoxBlur;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;


public class CirclePane extends Group{

	
	public CirclePane() {
		super();

		BoxBlur blur = new BoxBlur();
		blur.setIterations(100);
		this.setEffect(blur);

		this.setBlendMode(BlendMode.HARD_LIGHT);

		this.parentProperty().addListener( (observeParent,oldParent,newParent)->{

			if(newParent instanceof StackPane) {
				
				StackPane stackPane = (StackPane)newParent;

				stackPane.widthProperty().addListener((observeNum,oldNum,newNum)->{
					this.resetCircles(stackPane.getWidth(), stackPane.getHeight());

				});

				stackPane.heightProperty().addListener((observeNum,oldNum,newNum)->{
					this.resetCircles(stackPane.getWidth(), stackPane.getHeight());

				});

			}
			
		});

	}



	private void resetCircles(double maxX, double maxY) {
		
		this.getChildren().clear();
		this.initCircles(maxX, maxY);
		
	}

	protected void initCircles(double maxX, double maxY) {
		
		if(maxY==0||maxX==0) {
			return;
		}

		final double rad = 50;
		int numCircles = (int)(maxX*maxY/300);
		
		for (int i = 0; i < numCircles; i++) {
			double x = Math.random()*(maxX-2.5*rad);
			double y = Math.random()*(maxY-2.5*rad);

			Color inside = Color.hsb(190, 0.5*Math.random() + 0.5, (3.0*(maxY-y)/maxY+Math.random())/4, 0.03);

			Circle circle = new Circle(50, inside);

			circle.setTranslateX(x);
			circle.setTranslateY(y);

			this.getChildren().add(circle);
		}
		
	}
	
	
}
