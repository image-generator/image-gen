package imagegen.nodes;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

public class TranslucentButton extends Button{

	public final static Background offBg = new Background(new BackgroundFill[] {
			new BackgroundFill(new Color(0.5,0.5,0.5,0.5),new CornerRadii(0.1,true),new Insets(2)),
	});

	public static final Background onBg = new Background(new BackgroundFill[] {
			new BackgroundFill(new Color(1,1,1,0.5),new CornerRadii(0.1,true),new Insets(2)),

	});
	
	public TranslucentButton(String name) {
		super(name);
		
		
		this.setTextFill(Color.WHITE);
		this.setBackground(offBg);

		this.setOnMouseEntered((event)->{
			this.setBackground(onBg);
		});

		this.setOnMouseExited((event)->{
			this.setBackground(offBg);
		});
		
		
	}
}
