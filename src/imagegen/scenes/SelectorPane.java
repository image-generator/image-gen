package imagegen.scenes;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.function.Consumer;

import imagegen.nodes.GenClassBox;
import imagegen.reflection.GenClassReflect;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;


public class SelectorPane extends BorderPane{


	private GenClassBox genBox;
	
	public SelectorPane() {
		super();
	}


	public void init() {
		
		
		//Sets background to Bejing image
		FileInputStream stream;
		try {
			stream = new FileInputStream("assets/beijing.png");
			Image img = new Image(stream);
			BackgroundSize resize = new BackgroundSize(1.0d,1.0d,true,true,true,true);
			BackgroundImage bgimg = new BackgroundImage(img,BackgroundRepeat.NO_REPEAT,BackgroundRepeat.NO_REPEAT,BackgroundPosition.CENTER,resize);
			this.setBackground(new Background(bgimg));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		//Title
		BorderPane leftBox = new BorderPane();

		HBox titleBox = new HBox();
		titleBox.setBackground(new Background(new BackgroundFill[] {new BackgroundFill(new Color(0.1,0.1,0.1,0.5),null,null)}));
		titleBox.setAlignment(Pos.CENTER);
		titleBox.setPadding(new Insets(0,10,10,10));
		titleBox.setBorder(new Border(new BorderStroke[] {new BorderStroke(new Color(0,0,0,0.1),BorderStrokeStyle.SOLID,null,new BorderWidths(0,0,3,0))}));
		
		Label title = new Label("Image Generator");
		title.setAlignment(Pos.TOP_CENTER);
		title.setTextAlignment(TextAlignment.CENTER);
		title.setFont(new Font("Calibri",48));
		title.setTextFill(Color.WHITE);
		
		title.setBorder(new Border(new BorderStroke[] {new BorderStroke(
				null,
				null,
				Color.WHITE,
				null,
				null,
				null,
				BorderStrokeStyle.SOLID,
				null,
				new CornerRadii(0.15,true),
				null,
				null
				)}));
		
		titleBox.getChildren().add(title);
		leftBox.setTop(titleBox);
		
		HBox bottomBox = new HBox();
		bottomBox.setAlignment(Pos.BASELINE_LEFT);
		bottomBox.setPadding(new Insets(5));
		
		Label signature = new Label("Written by Samuel Segal");
		signature.setBackground(new Background(new BackgroundFill[] {new BackgroundFill(new Color(0.1,0.1,0.1,0.5),null,null)}));
		signature.setTextFill(Color.WHITE);
		
		bottomBox.getChildren().add(signature);
		leftBox.setBottom(bottomBox);
		
		this.setCenter(leftBox);


		//Right side
		VBox rightBox = new VBox();
		rightBox.setBackground(new Background(new BackgroundFill[] {new BackgroundFill(new Color(0.1,0.1,0.1,0.75),null,null)}));
		rightBox.setBorder(new Border(new BorderStroke[] {new BorderStroke(new Color(0,0,0,0.3),BorderStrokeStyle.SOLID,null,new BorderWidths(0,10,0,2))}));
		rightBox.setPadding(new Insets(18,20,20,10));

		Label topLabel = new Label("Render Classes");
		topLabel.setFont(new Font("Calibri",30.0));
		topLabel.setTextFill(Color.WHITE);
		topLabel.setBorder(new Border(new BorderStroke[] {new BorderStroke(null,null,Color.WHITE,null,null,null,BorderStrokeStyle.SOLID,null,new CornerRadii(0.15,true),null,null)}));

		this.genBox = new GenClassBox();

		rightBox.getChildren().addAll(topLabel,this.genBox);

		this.setRight(rightBox);
		
	}


	public void setButtonPress(Consumer<GenClassReflect> consumer) {
		this.genBox.setButtonPress(consumer);
	}


}
