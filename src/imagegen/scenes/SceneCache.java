package imagegen.scenes;

import java.util.HashMap;
import java.util.Map;

import imagegen.reflection.GenClassReflect;


public class SceneCache {
	
	
	protected Map<GenClassReflect,RenderPane> sceneMap;
	
	public SceneCache() {
		this.sceneMap = new HashMap<GenClassReflect,RenderPane>();
	}
	
	public RenderPane initScene(GenClassReflect genClass) {
		RenderPane scene = new RenderPane(genClass);
		scene.init();
		this.sceneMap.put(genClass, scene);
		return scene;
	}
	
	public RenderPane getScene(GenClassReflect genClass) {
		if(!sceneMap.containsKey(genClass)) {
			return this.initScene(genClass);
		}
		return sceneMap.get(genClass);
	}
	
	public void clearScenes() {
		for(RenderPane scene: sceneMap.values()) {
			scene.getRenderer().stop();
		}
	}
}
