package imagegen.scenes;

import java.util.function.Consumer;

import imagegen.nodes.ButtonPane;
import imagegen.nodes.CirclePane;
import imagegen.nodes.ParamPane;
import imagegen.nodes.ResizableImageContainer;
import imagegen.nodes.TranslucentButton;
import imagegen.paramfields.ParamField;
import imagegen.reflection.GenClassReflect;
import imagegen.reflection.GenParamReflect;
import imagegen.reflection.ParamGetter;
import imagegen.render.Renderer;
import javafx.geometry.Insets;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;


public class RenderPane extends BorderPane{

	private static final Background darkBg = new Background(new BackgroundFill[] {new BackgroundFill(new Color(0.15,0.15,0.2,1),null,null)});
	
	
	
	
	private Consumer<MouseEvent> onBack = System.out::println;
	protected ParamField[] paramFields;
	protected GenClassReflect genClass;
	protected Renderer renderer;

	
	
	
	public RenderPane(GenClassReflect genClass) {
		super();
		this.genClass = genClass;
	}

	

	private void initializeParamGetters() {

		GenParamReflect[] params = this.genClass.getGenParams();
		this.paramFields = new ParamField[params.length];

		for(int i=0;i<this.paramFields.length;i++) {
			GenParamReflect parameter = params[i];

			try {

				ParamField field = ParamGetter.getParamFieldFromClass(parameter.getType(),parameter.getName());
				this.paramFields[i] = field;

			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

		}
	}

	
	public void init() {

		this.renderer = new Renderer(this.genClass);
		this.initializeParamGetters();

		
		
		VBox topBox = new VBox();
		topBox.setBackground(darkBg);
		
		TranslucentButton backButton = new TranslucentButton("Back");
		backButton.setOnMouseClicked((event)->{
			this.onBackButton(event);
		});

		topBox.getChildren().add(backButton);
		
		this.setTop(topBox);
		
		
		
		StackPane stacker = new StackPane();
		stacker.setBackground(darkBg);
		
		CirclePane circlePane = new CirclePane();
		
		BorderPane controlPane = new BorderPane();
		controlPane.setPadding(new Insets(20));
		
		ParamPane paramPane = new ParamPane(this.paramFields);
		controlPane.setTop(paramPane);
		
		ButtonPane buttonPane = new ButtonPane(this.renderer,this.paramFields);
		controlPane.setBottom(buttonPane);

		stacker.getChildren().addAll(circlePane,controlPane);
		
		this.setRight(stacker);



		ResizableImageContainer imageContainer = new ResizableImageContainer();
		this.renderer.setOnImageUpdate((image)->{
			imageContainer.setImage(image);
		});

		this.setCenter(imageContainer);
	}

	
	
	public void onBackButton(MouseEvent event) {
		onBack.accept(event);
	}

	public void setOnBack(Consumer<MouseEvent> setOnBack) {
		this.onBack = setOnBack;
	}

	public Renderer getRenderer() {
		return renderer;
	}



}
