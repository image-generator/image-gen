package imagegen;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import imagegen.reflection.ReflectCache;
import imagegen.scenes.RenderPane;
import imagegen.scenes.SceneCache;
import imagegen.scenes.SelectorPane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


public class App extends Application {


	public static ReflectCache reflectCache = new ReflectCache();
	public static SceneCache sceneCache = new SceneCache();



	@Override
	public void start(Stage stage) {

		SelectorPane selectorPane = new SelectorPane();
		selectorPane.init();

		Scene scene = new Scene(selectorPane);

		selectorPane.setButtonPress((genClass)->{
			RenderPane renderPane = sceneCache.getScene(genClass);
			renderPane.setOnBack((event)->{
				scene.setRoot(selectorPane);
			});
			scene.setRoot(renderPane);
		});


		//Sets the icon
		try {
			FileInputStream stream = new FileInputStream("assets/icon.png");
			Image img = new Image(stream);
			stage.getIcons().add(img);
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}

		stage.setTitle("Image Generator");
		stage.setMaximized(true);
		stage.setScene(scene);
		stage.show();

		//Ties up loose threads, literally
		stage.setOnCloseRequest((event)->{
			sceneCache.clearScenes();
		});

	}

	public static void main(String[] args) {
		reflectCache.initializeClasses();
		App.launch();
	}

	
}